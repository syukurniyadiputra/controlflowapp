package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener(this)

        // TODO 1: Username & password tidak boleh empty
        // TODO 2: Password minimal 7 digit
        // TODO 3: akan sukses jika login: user@mail.com, password
        // TODO 4: username mengecek format email
    }

    override fun onClick(view: View) {
        if (checkInput()) {
            if (etUsername.text.toString() == "user@mail.com" && etPassword.text.toString() == "password") {
                statusLogin.setText("Status Login: Berhasil")
            } else {
                statusLogin.setText("Status Login: Gagal")
            }
        }
    }

    private val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
        ("[a-zA-Z0-9._-]+@[a-z]+\\\\.+[a-z]+")
    )

    private fun checkEmail(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

    private fun checkInput(): Boolean {
        when {
            etUsername.length() == 0 -> {
                etUsername.setError("username kosong")
                return false
            }
            etPassword.length() == 0 -> {
                etPassword.setError("password kosong")
                return false
            }
            checkEmail(etUsername.text.toString()) == false -> {
                etUsername.setError("username salah format email")
                return false
            }
            etPassword.length() < 7 -> {
                etPassword.setError("password kurang dari 7")
                return false
            }
            else -> return true
        }
    }
}
