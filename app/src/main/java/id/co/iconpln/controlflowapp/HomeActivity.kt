package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.backgroundThread.BackgroundThreadActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.GridHeroActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.sharedPreference.SharedPreferenceActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnCalculation.setOnClickListener(this)
        btnClassification.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
        btnOperation.setOnClickListener(this)
        btnStyle.setOnClickListener(this)
        btnActivity.setOnClickListener(this)
        btnVolumeActivity.setOnClickListener(this)
        btnIntentActivity.setOnClickListener(this)
        btnConstraintActivity.setOnClickListener(this)
        btnComplexConstraintActivity.setOnClickListener(this)
        btnListHeroConstActivity.setOnClickListener(this)
        btnGridHeroActivity.setOnClickListener(this)
        btnDemoFragment.setOnClickListener(this)
        btnTabFragment.setOnClickListener(this)
        btnBottomNav.setOnClickListener(this)
        btnNavDrawer.setOnClickListener(this)
        btnBottomSheet.setOnClickListener(this)
        btnLocalization.setOnClickListener(this)
        btnScroll.setOnClickListener(this)
        btnSharedPreferences.setOnClickListener(this)
        btnWeather.setOnClickListener(this)
        btnContact.setOnClickListener(this)
        btnContactTab.setOnClickListener(this)
        btnBackgroundThread.setOnClickListener(this)
        btnMyContact.setOnClickListener(this)
        btnMyUser.setOnClickListener(this)
        btnMyProfile.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnCalculation -> {
                val calculationIntent = Intent(this, MainActivity::class.java)
                startActivity(calculationIntent)
            }
            R.id.btnClassification -> {
                val classificationIntent = Intent(this, ClassificationActivity::class.java)
                startActivity(classificationIntent)
            }
            R.id.btnLogin -> {
                val loginIntent = Intent(this, LoginActivity::class.java)
                startActivity(loginIntent)
            }
            R.id.btnOperation -> {
                val operationIntent = Intent(this, OperationActivity::class.java)
                startActivity(operationIntent)
            }
            R.id.btnStyle -> {
                val styleIntent = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnActivity -> {
                val activityIntent = Intent (this, DemoActivity::class.java)
                startActivity(activityIntent)
            }
            R.id.btnVolumeActivity -> {
                val volumeIntent = Intent (this, VolumeActivity::class.java)
                startActivity(volumeIntent)
            }
            R.id.btnIntentActivity -> {
                val intentIntent = Intent (this, IntentActivity::class.java)
                startActivity(intentIntent)
            }
            R.id.btnConstraintActivity -> {
                val constraint = Intent (this, ComplexConstraintActivity::class.java)
                startActivity(constraint)
            }
            R.id.btnComplexConstraintActivity -> {
                val complex = Intent (this, ConstraintActivity::class.java)
                startActivity(complex)
            }
            R.id.btnListHeroConstActivity -> {
                val listHeroCons = Intent (this, ListHeroActivity::class.java)
                startActivity(listHeroCons)
            }
            R.id.btnGridHeroActivity -> {
                val gridHero = Intent (this, GridHeroActivity::class.java)
                startActivity(gridHero)
            }
            R.id.btnDemoFragment -> {
                val demoFragment = Intent (this, DemoFragmentActivity::class.java)
                startActivity(demoFragment)
            }
            R.id.btnTabFragment -> {
                val tabFragment = Intent (this, TabActivity::class.java)
                startActivity(tabFragment)
            }
            R.id.btnBottomNav -> {
                val navFragment = Intent(this, BottomNavActivity::class.java)
                startActivity(navFragment)
            }
            R.id.btnNavDrawer -> {
                val navDrawer = Intent (this, NavDrawerActivity::class.java)
                startActivity(navDrawer)
            }
            R.id.btnBottomSheet -> {
                val bottomSheet = Intent (this, BottomSheetActivity::class.java)
                startActivity(bottomSheet)
            }
            R.id.btnLocalization -> {
                val localization = Intent (this, LocalizationActivity::class.java)
                startActivity(localization)
            }
            R.id.btnScroll -> {
                val scroll = Intent(this, ScrollActivity::class.java)
                startActivity(scroll)
            }
            R.id.btnSharedPreferences -> {
                val sharedPreferences = Intent(this, SharedPreferenceActivity::class.java)
                startActivity(sharedPreferences)
            }
            R.id.btnWeather -> {
                val weather = Intent(this, WeatherActivity::class.java)
                startActivity(weather)
            }
            R.id.btnContact -> {
                val contact = Intent(this, ContactActivity::class.java)
                startActivity(contact)
            }
            R.id.btnContactTab -> {
                val contactTab = Intent(this, ContactTabActivity::class.java)
                startActivity(contactTab)
            }
            R.id.btnBackgroundThread -> {
                val backgroundThread = Intent(this, BackgroundThreadActivity::class.java)
                startActivity(backgroundThread)
            }
            R.id.btnMyContact -> {
                val myContact = Intent(this, MyContactActivity::class.java)
                startActivity(myContact)
            }
            R.id.btnMyUser -> {
                val myUser = Intent(this, MyUserActivity::class.java)
                startActivity(myUser)
            }
            R.id.btnMyProfile -> {
                val myProfile = Intent(this, MyProfileActivity::class.java)
                startActivity(myProfile)
            }
        }
    }
}
