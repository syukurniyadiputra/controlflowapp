package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_operation.*

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private var inputX: Int = 0
    private var inputY: Int = 0

    private lateinit var operationViewModel: OperationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        initViewModel()
        displayResult()
        setButtonClickListener()
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun displayResult() {
        tvOpResult.text = operationViewModel.operationResult.toString()
        tvOperator.text = operationViewModel.operationString

    }

    private fun setButtonClickListener() {
        btnOpAdd.setOnClickListener(this)
        btnOpDivide.setOnClickListener(this)
        btnOpMultiply.setOnClickListener(this)
        btnOpSubtract.setOnClickListener(this)
        btnReset.setOnClickListener(this)
    }

    private fun getInputNumbers() {
        if (etBilanganX.text?.isNotEmpty() == true ||
            etBilanganY.text?.isNotEmpty() == true
        ) {
            inputX = etBilanganX.text.toString().toInt()
            inputY = etBilanganY.text.toString().toInt()
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnOpAdd -> {
                if (validate()) {
                    toastMessage()
                } else {
                    operationViewModel.operationString = getString(R.string.et_operation_tambah)
                    getInputNumbers()
                    val operation = Operation.Add(inputX)
                    operationViewModel.execute(inputY, operation)
                    displayResult()
                }
            }
            R.id.btnOpDivide -> {
                if (validate() || etBilanganX.text.toString().toInt() == 0 && etBilanganY.text.toString().toInt() == 0
                ) {
                    toastMessage()
                } else {
                    operationViewModel.operationString = getString(R.string.et_operation_bagi)
                    getInputNumbers()
                    val operation = Operation.Divide(inputX)
                    operationViewModel.execute(inputY, operation)
                    displayResult()
                }
            }
            R.id.btnOpMultiply -> {
                if (validate()) {
                    toastMessage()
                } else {
                    operationViewModel.operationString = getString(R.string.et_operation_kali)
                    getInputNumbers()
                    val operation = Operation.Multiply(inputX)
                    operationViewModel.execute(inputY, operation)
                    displayResult()
                }
            }
            R.id.btnOpSubtract -> {
                if (validate()) {
                    toastMessage()
                } else {
                    operationViewModel.operationString = getString(R.string.et_operation_kurang)
                    getInputNumbers()
                    val operation = Operation.Substract(inputX)
                    operationViewModel.execute(inputY, operation)
                    displayResult()
                }
            }
            R.id.btnReset -> {
                etBilanganX.setText("0")
                etBilanganY.setText("0")
                operationViewModel.operationString = ""
                operationViewModel.operationResult = 0
                displayResult()
            }
        }
    }

    private fun toastMessage() {
        Toast.makeText(this, "X or Y Blank", Toast.LENGTH_SHORT).show()
    }

    private fun validate(): Boolean {
        return etBilanganX.text.isNullOrEmpty() || etBilanganX.text.isNullOrBlank() || etBilanganY.text.isNullOrEmpty() || etBilanganY.text.isNullOrBlank()
    }
}
