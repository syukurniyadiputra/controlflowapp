package id.co.iconpln.controlflowapp.fragments


import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import id.co.iconpln.controlflowapp.StyleActivity

import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_last.*

/**
 * A simple [Fragment] subclass.
 */
class LastFragment : Fragment(), View.OnClickListener {
    
    companion object {
        const val EXTRA_NAME_FRAGMENT = "extra_name_fragment"
        var message: String = ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_last, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val name = arguments?.getString(EXTRA_NAME_FRAGMENT)
        tvLastName.text = name
        tvLastMsg.text = message

        btnOtherActivity.setOnClickListener(this)
        btnShowActivity.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnOtherActivity -> {
                val styleActivity = Intent(requireContext(), StyleActivity::class.java)
                startActivity(styleActivity)
            }
            R.id.btnShowActivity -> {
                val fragmentManager = childFragmentManager
                val optionalDialogFragment = OptionDialogFragment()

                optionalDialogFragment.show(
                    fragmentManager, OptionDialogFragment::class.java.simpleName
                )
            }
        }
    }

    var optionsDialogListener: OptionDialogFragment.OnOptionsDialogListener =
        object : OptionDialogFragment.OnOptionsDialogListener{
            override fun onOptionChosen(text: String) {
                Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
            }
        }
}
