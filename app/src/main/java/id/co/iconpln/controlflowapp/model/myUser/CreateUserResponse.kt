package id.co.iconpln.controlflowapp.model.myUser

data class CreateUserResponse<T>(
    val message: String,
    val created_users: UserDataResponse
)