package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserFavorite.MyUserFavoriteActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity.Companion.EXTRA_USER_EDIT
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity.Companion.EXTRA_USER_ID
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter

    private lateinit var myUserViewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        initViewModel()
        showListUser()
        addListClickListener()
        fabMyUserAdd.setOnClickListener(this)
        fetchUserData()
    }

    override fun onResume() {
        super.onResume()
        fetchUserData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_favorite_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_favorite_list) {
            val favoriteIntent = Intent(this, MyUserFavoriteActivity::class.java)
            startActivity(favoriteIntent)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addListClickListener() {
        adapter.setOnItemClickCallback(object : MyUserAdapter.OnItemClickCallback{
            override fun onItemClick(myUser: UserDataResponse) {
                Toast.makeText(this@MyUserActivity, "Your choose ${myUser.name} ${myUser.address} ${myUser.phone}", Toast.LENGTH_LONG).show()
                openUserForm(myUser)
            }
        })
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.fabMyUserAdd -> {
                val userFormAddIntent = Intent(this, MyUserFormActivity::class.java)
                startActivity(userFormAddIntent)
            }
        }
    }

    fun openUserForm(myUser : UserDataResponse) {
        val userFormIntent = Intent(this@MyUserActivity, MyUserFormActivity::class.java)
//        userFormIntent.putExtra(EXTRA_USER, myUser)
        userFormIntent.putExtra(EXTRA_USER_ID, myUser.id)
        userFormIntent.putExtra(EXTRA_USER_EDIT, true)
        startActivity(userFormIntent)
    }

    private fun fetchUserData() {
        // get value from view model's Live Dat
        myUserViewModel.getListUsers().observe(this, Observer { contactItem ->
            if (contactItem != null) {
                adapter.setData(contactItem)
            }
        })
    }

    private fun initViewModel() {
        myUserViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())
            .get(MyUserViewModel::class.java)
    }

    private fun showListUser() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter
    }
}
