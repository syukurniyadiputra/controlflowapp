package id.co.iconpln.controlflowapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*

class VolumeActivity : AppCompatActivity() {

    private lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

        initViewModel()
        displayResult()
        setClickListener()
    }

    private fun initViewModel() {
        volumeViewModel =  ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }

    private fun displayResult() {
        tvResultVolume.text = volumeViewModel.volumeResult.toString()
    }

    private fun setClickListener() {
        btnVolume.setOnClickListener {
            val length = etLength.text.toString()
            val width = etWidth.text.toString()
            val height = etHeight.text.toString()

            when {
                length.isEmpty() -> etLength.error = "Empty Field"
                width.isEmpty() -> etWidth.error = "Empty Field"
                height.isEmpty() -> etHeight.error = "Empty Field"
                else -> {
                    volumeViewModel.calculate(length,width,height)
                    displayResult()
                }
            }
        }
    }
}
