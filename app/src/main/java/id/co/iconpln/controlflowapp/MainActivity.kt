package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    var operation = arrayOf("Perkalian", "Pembagian", "Pertambahan", "Pengurangan")
    var spinner: Spinner? = null
    var opSelection = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etNilai1.setText("0")
        etNilai2.setText("0")

        spinner = this.etOperation
        spinner!!.setOnItemSelectedListener(this)

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, operation)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner!!.setAdapter(aa)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnShow.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        btnShow.setOnClickListener {
            if (etNilai1.text.isNotEmpty() || etNilai2.text.isNotEmpty()) {
                val angka1 = etNilai1.text.toString().toInt()
                val angka2 = etNilai2.text.toString().toInt()
                when(opSelection) {
                    "Perkalian" -> hitungPerkalian(angka1, angka2)
                }
            }
        }
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        opSelection = operation[position]
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
        opSelection = ""
    }

    fun hitungPerkalian(nilai1: Int, nilai2: Int) {
        val hitungPerkalian = nilai1 * nilai2
        etHasil.setText("Hasilnya: $hitungPerkalian")
    }


}
