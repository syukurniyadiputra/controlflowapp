package id.co.iconpln.controlflowapp.database

import androidx.lifecycle.LiveData

class FavoriteDatabaseRepository(private val favoriteDatabaseDao: FavoriteDatabaseDao) {

    val allFavUsers: LiveData<List<FavoriteUser>> = favoriteDatabaseDao.getAllUsers()

    fun insertUser(user: FavoriteUser) {
        favoriteDatabaseDao.insertUser(user)
    }

    fun deleteUser(id : Int) {
        favoriteDatabaseDao.deleteUser(id)
    }

    fun getUser(id : Int) : LiveData<FavoriteUser> {
        return favoriteDatabaseDao.getFavUser(id)
    }

    fun updateUser(user: FavoriteUser) {
        return favoriteDatabaseDao.updateUser(user)
    }
}