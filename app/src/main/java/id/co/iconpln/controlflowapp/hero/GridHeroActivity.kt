package id.co.iconpln.controlflowapp.hero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero.*

class GridHeroActivity : AppCompatActivity() {

    private var listHero: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero)

        setupListHero()
        showRecycleGrid()
    }

    private fun setupListHero() {
        rvGridHero.setHasFixedSize(true)
        listHero.addAll(HeroesData.listDataHero)

        setupGridDivider()
    }

    private fun setupGridDivider() {
        val dividerItemDecoration = DividerItemDecoration(
            rvGridHero.context, DividerItemDecoration.VERTICAL)
        rvGridHero.addItemDecoration(dividerItemDecoration)
    }

    private fun showRecycleGrid() {
        rvGridHero.layoutManager = GridLayoutManager(this, 3)
        val gridHeroAdapter = GridHeroAdapter(listHero)
        rvGridHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnItemClickCallback(object : GridHeroAdapter.OnItemClickCallback{
            override  fun OnItemClick(hero: Hero) {
                Toast.makeText(this@GridHeroActivity, "Your choose ${hero.name}", Toast.LENGTH_LONG).show()
            }
        })    }
}
