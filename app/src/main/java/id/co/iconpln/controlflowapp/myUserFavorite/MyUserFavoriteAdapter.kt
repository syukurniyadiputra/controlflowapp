package id.co.iconpln.controlflowapp.myUserFavorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import kotlinx.android.synthetic.main.item_list_my_user.view.*

class MyUserFavoriteAdapter : RecyclerView.Adapter<MyUserFavoriteAdapter.MyUserViewHolder>() {

    private var userData = emptyList<FavoriteUser>()

    private lateinit var onItemClickCallback: OnItemClickCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_my_user, parent, false)
        return MyUserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userData.size
    }

    override fun onBindViewHolder(holder: MyUserViewHolder, position: Int) {
        holder.bind(userData[position])
        holder.itemView.setOnClickListener {
            onItemClickCallback.onItemClick(userData[holder.adapterPosition])
        }
    }

    fun setData(userItems: List<FavoriteUser>) {
        val listFavUser = ArrayList<FavoriteUser>()
        for (i in 0 until userItems.size) {
            listFavUser.add(userItems[i])
        }
        userData = listFavUser
        notifyDataSetChanged()
    }

    inner class MyUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contactItem: FavoriteUser) {
            itemView.tvMyUserName.text = contactItem.userName
            itemView.tvMyUserAddress.text = contactItem.userAddress
            itemView.tvMyUserMobile.text = contactItem.userPhone
        }
    }

    fun setOnItemClickCallback (onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(myUser: FavoriteUser)
    }
}