package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent_object.*

class IntentObjectActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PERSON = "extra_person"
    }

    lateinit private var person: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_object)

        getIntentExtras()
        showData()
    }

    private fun getIntentExtras() {
        person = intent.getParcelableExtra(EXTRA_PERSON)
    }

    private fun showData() {
        val text = "Nama : ${person.name}, Age :  ${person.age}, Email : ${person.email} , City : ${person.city}"
        tvObjectReceived.text = text
    }
}
