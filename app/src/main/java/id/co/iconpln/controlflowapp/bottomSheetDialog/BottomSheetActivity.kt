package id.co.iconpln.controlflowapp.bottomSheetDialog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_bottom_sheet.view.*
import kotlinx.android.synthetic.main.layout_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_content_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class BottomSheetActivity : AppCompatActivity(), View.OnClickListener, BottomSheetFragment.ItemClickListener {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)

        setupActionBar()
        setButtonClickListener()
        setupBottomSheetBehavior()
    }

    private fun setupBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(
            object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(view: View, slideOffset: Float) {

                }

                override fun onStateChanged(view: View, newState: Int) {
                    when(newState) {
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            btnBottomSheet.text = "Expand Bottom Sheet"
                        }
                        BottomSheetBehavior.STATE_DRAGGING -> {}
                        BottomSheetBehavior.STATE_EXPANDED -> {
                            btnBottomSheet.text = "Close Bottom Sheet"
                        }
                        BottomSheetBehavior.STATE_HALF_EXPANDED -> {}
                        BottomSheetBehavior.STATE_HIDDEN -> {}
                        BottomSheetBehavior.STATE_SETTLING -> {}
                    }
                }

            }
        )
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
    }

    private fun setButtonClickListener() {
        btnBottomSheet.setOnClickListener(this)
        btnBottomSheetDialog.setOnClickListener(this)
        btnBottomDialogFragment.setOnClickListener(this)
        btnBottomPayment.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btnBottomSheet -> {
                if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    btnBottomSheet.text = "Close Bottom Sheet"
                }
            }
            R.id.btnBottomSheetDialog -> {
                val dialogView = layoutInflater.inflate(R.layout.fragment_bottom_sheet, null)
                val bottomSheetDialog = BottomSheetDialog(this)
                setDialogClickListener(dialogView)
                bottomSheetDialog.setContentView(dialogView)
                bottomSheetDialog.show()
            }
            R.id.btnBottomDialogFragment -> {
                val bottomSheetFragment = BottomSheetFragment()
                bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
            }
            R.id.btnBottomPayment -> {
                tvBottomActivity.setText("Order is Paid")
            }
        }
    }
    private fun setDialogClickListener(dialogView: View) {
        dialogView.llBottomPreview.setOnClickListener {
            val textPreview = dialogView.tvBottomPreview.text.toString()
            onItemClick("Dialog $textPreview")
        }
        dialogView.llBottomShare.setOnClickListener {
            val textShare = dialogView.tvBottomShare.text.toString()
            onItemClick("Dialog $textShare")
        }
        dialogView.llBottomEdit.setOnClickListener {
            val textEdit = dialogView.tvBottomEdit.text.toString()
            onItemClick("Dialog $textEdit")
        }
        dialogView.llBottomSearch.setOnClickListener {
            val textSearch = dialogView.tvBottomSearch.text.toString()
            onItemClick("Dialog $textSearch")
        }
        dialogView.llBottomExit.setOnClickListener {
            val textExit = dialogView.tvBottomExit.text.toString()
            onItemClick("Dialog $textExit")
        }
    }

    override fun onItemClick(text: String) {
        tvBottomActivity.text = text
    }
}
