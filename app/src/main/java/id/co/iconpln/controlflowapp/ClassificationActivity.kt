package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_classification.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        etClaNilai1.setText("0")
        btnClaShow.setOnClickListener {
            if (etClaNilai1.text.isNotEmpty()) {
                doClassification(etClaNilai1.text.toString().toInt())
            }
        }
    }

    fun doClassification(nilai: Int) {
        // TODO Nilai 0-70 Tidak Lulus
        // TODO Nilai 71-80 Lulus Aja
        // TODO Nilai 81-100 Lulus Banget
        // TODO Selain itu Nilai Error
        // Kalau input empty, tidak boleh crash
        // Nilai maksimal yang bisa diinput 1000

        var hasil = ""
        if (nilai <= 1000) {
            when (nilai) {
                in 0..70 -> {hasil = "Tidak Lulus"}
                in 71..80 -> {hasil = "Lulus Aja"}
                in 81..100 -> {hasil = "Lulus Banget"}
                !in 0..100 -> {hasil = "Nilai Error"}
            }
            etClaHasil.text = hasil
        } else {
            Toast.makeText(this, "Lebih Dari 1000", Toast.LENGTH_LONG).show()
        }
    }
}
