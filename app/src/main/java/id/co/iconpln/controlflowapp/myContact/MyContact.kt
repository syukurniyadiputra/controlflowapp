package id.co.iconpln.controlflowapp.myContact

class MyContact (
    var id: String? = "0",
    var name: String? = null,
    var email: String? = null,
    var mobile: String? = null
)