package id.co.iconpln.controlflowapp.backgroundThread

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.fragments.LastFragment.Companion.message
import kotlinx.android.synthetic.main.activity_background_thread.*
import kotlinx.coroutines.*
import java.lang.Runnable
import java.lang.ref.WeakReference
import java.net.URL

class BackgroundThreadActivity : AppCompatActivity(), View.OnClickListener, ContactAsyncTaskCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_background_thread)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnThreadWorker.setOnClickListener(this)
        btnThreadHandler.setOnClickListener(this)
        btnThreadAsyncTask.setOnClickListener(this)
        btnThreadCoroutine.setOnClickListener(this)
        btnThreadAsyncCoroutine.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnThreadWorker -> {
                /*
                Don't call network in Main Thread
                val text = URL("https://api.androidhive.info/contacts/").readText()
                tvThreadWorkerResult.text = text
                 */

                Thread(Runnable {
                    val contactResultText = URL("https://api.androidhive.info/contacts/").readText()
                    // Don't call UI thread in background
                    // tvThreadWorkerResult.text = contactResultText
                    tvThreadWorkerResult.post(Runnable {
                        tvThreadWorkerResult.text = contactResultText
                    })
                }).start()
            }
            R.id.btnThreadHandler -> {
                Thread(Runnable {
                    val contactResultText = URL("https://api.androidhive.info/contacts/").readText()
                    val msg = Message.obtain()
                    msg.obj = contactResultText
                    msg.target = contactHandler
                    msg.sendToTarget()
                }).start()
            }
            R.id.btnThreadAsyncTask -> {
                val contactUrl = URL("https://api.androidhive.info/contacts/")
                FetchContactAsyncTask(this).execute(contactUrl)
            }
            R.id.btnThreadCoroutine -> {
                runBlocking {
                    launch {
                        delay(1000)
                        tvThreadCoroutineResult.text = "Coroutine!"
                    }
                }
            }
            R.id.btnThreadAsyncCoroutine -> {
                /*
                runBlocking {
                    val numberAsync = async { getNumber() }
                    val result = numberAsync.await()
                    tvThreadCoroutineAsyncResult.text = result.toString()
                }
                */
                 runBlocking {
                     val contactAsync = async { getContact() }
                     val result = contactAsync.await()
                     tvThreadCoroutineAsyncResult.text = result
                 }
            }
        }
    }

    suspend fun getNumber(): Int {
        delay(1000)
        return 3*2
    }

    suspend fun getContact(): String {
        return withContext(Dispatchers.IO) {
            URL("https://api.androidhive.info/contacts/").readText()
        }
    }

    private val contactHandler = Handler() { message ->
        tvThreadHandlerResult.text = message.obj as String
        true
    }

    override fun onPreExecute() {
        pbThreadAsyncProgress.visibility = View.VISIBLE
        tvThreadAsyncResult.visibility = View.GONE
    }

    override fun onProgressUpdate(vararg values: Int?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPostExecute(result: String?) {
        pbThreadAsyncProgress.visibility = View.GONE
        tvThreadAsyncResult.visibility = View.VISIBLE
        tvThreadAsyncResult.text = result
    }

    class FetchContactAsyncTask(val listener: ContactAsyncTaskCallback): AsyncTask<URL, Int, String>() {

        // using WeakReference to avoid memory leak in AsyncTask
        private val contactListener: WeakReference<ContactAsyncTaskCallback>
            = WeakReference(listener)

        override fun onPreExecute() {
            super.onPreExecute()

            val myListener = contactListener.get()
            myListener?.onPreExecute()
        }

        override fun doInBackground(vararg urls: URL): String {
            val urlResult = urls[0].readText()
            return urlResult
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)

            val myListener = contactListener.get()
            myListener?.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            val myListener = contactListener.get()
            myListener?.onPostExecute(result)
        }

    }
}

interface ContactAsyncTaskCallback {
    fun onPreExecute()
    fun onProgressUpdate(vararg values: Int?)
    fun onPostExecute(result: String?)
}
