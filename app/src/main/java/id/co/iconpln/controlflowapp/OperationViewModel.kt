package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class OperationViewModel: ViewModel() {

    var operationResult: Int = 0
    var operationString: String = ""

    fun execute(x: Int, operation: Operation) {
        operationResult = when (operation) {
            is Operation.Add -> operation.value + x
            is Operation.Divide -> operation.value / x
            is Operation.Multiply -> operation.value * x
            is Operation.Substract -> operation.value - x
        }
    }


}