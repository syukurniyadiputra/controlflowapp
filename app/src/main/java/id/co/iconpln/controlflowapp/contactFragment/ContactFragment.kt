package id.co.iconpln.controlflowapp.contactFragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.contactFragment.ContactFragmentAdapter

import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_contact.*
import kotlinx.android.synthetic.main.activity_contact.rvContactList
import kotlinx.android.synthetic.main.fragment_contact.*

/**
 * A simple [Fragment] subclass.
 */
class ContactFragment : Fragment() {

    private lateinit var adapter : ContactFragmentAdapter

    private lateinit var contactFragmentViewModel: ContactFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        showListContact()
        contactFragmentViewModel.setContact()
        showLoading(true)
        fetchContactData()
    }

    private fun fetchContactData() {
        // get value from view model's Live Dat
        contactFragmentViewModel.getContact().observe(this, Observer { contactItem ->
            if (contactItem != null) {
                adapter.setData(contactItem)
                showLoading(false)
            }
        })    }

    private fun initViewModel() {
        contactFragmentViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())
            .get(ContactFragmentViewModel::class.java)
    }

    private fun showListContact() {
        adapter = ContactFragmentAdapter()
        adapter.notifyDataSetChanged()

        rvContactList.layoutManager = LinearLayoutManager(requireContext())
        rvContactList.adapter = adapter
    }

    private fun showLoading(state: Boolean){
        if(state){
            pbContactFragment.visibility = View.VISIBLE
        } else {
            pbContactFragment.visibility = View.GONE
        }
    }
}
