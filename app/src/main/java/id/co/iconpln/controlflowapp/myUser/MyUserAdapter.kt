package id.co.iconpln.controlflowapp.myUser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.item_list_my_user.view.*

class MyUserAdapter : RecyclerView.Adapter<MyUserAdapter.MyUserViewHolder>() {

    private val myUserData = ArrayList<UserDataResponse>()

    private lateinit var onItemClickCallback: OnItemClickCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_my_user, parent, false)
        return MyUserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myUserData.size
    }

    override fun onBindViewHolder(holder: MyUserViewHolder, position: Int) {
        holder.bind(myUserData[position])
        holder.itemView.setOnClickListener {
            onItemClickCallback.onItemClick(myUserData[holder.adapterPosition])
        }
    }

    fun setData(contactItem: ArrayList<UserDataResponse>) {
        myUserData.clear()
        myUserData.addAll(contactItem)
        notifyDataSetChanged()
    }

    inner class MyUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contactItem: UserDataResponse) {
            itemView.tvMyUserName.text = contactItem.name
            itemView.tvMyUserAddress.text = contactItem.address
            itemView.tvMyUserMobile.text = contactItem.phone
        }
    }

    fun setOnItemClickCallback (onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(myUser: UserDataResponse)
    }
}